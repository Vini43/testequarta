import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Amazonteste {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/Users/vinif/Downloads/geckodriverl-v0.30.0-win64/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAunten() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("shaushaus@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void vadilicaoLivro(){
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[6]")).click();
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Senhor dos aneis");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
    }

    @Test
    public void novidadesdaAmazon() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
    }

    @Test
    public void fireTV() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[7]/a/div")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[2]/li[5]/a")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("buy-now-button")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("ap_email")).sendKeys("shaushaus@gmail.com");
        Thread.sleep(3000);
        driver.findElement(By.id("continue")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void fogaodaAmazon() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click(); //  no menu
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[19]/a[1]")).click(); // no ver tudo
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/ul[1]/li[7]/a/div")).click(); // cozinha
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[17]/li[3]/a")).click(); // tudo em cozinha
        Thread.sleep(3000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("fogao de indução"); //escreve na pesquisa "fogao de induçao"
        Thread.sleep(3000);
        driver.findElement(By.id("nav-search-submit-button")).click(); //pesquisa
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[5]/div/div/div/div/div/div[2]/div/div/div[1]/h2/a/span")).click(); //Fogão Cooktop
        Thread.sleep(3000);
        Assert.assertEquals("Fogão Cooktop Fischer 2Q Indução Mesa Vitrocerâmica Preta 220V", driver.findElement(By.id("productTitle")).getText());
        Thread.sleep(3000);


    }


}
